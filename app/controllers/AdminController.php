<?php
use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;

class AdminController extends Controller
{

    public function indexAction() {
    	$auth = $this->session->get('auth');
        // var_dump($auth);die;
        if (!$auth) {
    	$this->view->mode = "login";
        } else {
    	$this->view->mode = "admin";
        $this->view->baseUrlApi = $this->config->application->baseUrlApi;
        }
    }

    public function loginAction() {
        // Check if the cookie has previously set
        if ($this->cookies->has('remember-me')) {

            // Get the cookie
            $rememberMe = $this->cookies->get('remember-me');

            // Get the cookie's value
            $value      = $rememberMe->getValue();

            $this->view->mode = "admin";
        }

    	$this->view->mode = "login";
    }

    public function startAction()
    {
        // var_dump("SDFE");die;
        if ($this->request->isPost()) {
            // var_dump($this->_callAPI("POST", $this->config->application->baseUrlApi. 'api/user/nativeLogin', $this->request->getPost()));die;
             $result = json_decode($this->_callAPI("POST", $this->config->application->baseUrlApi. 'api/user/nativeLogin', $this->request->getPost()));
             if( $result->content ){
             		$this->_registerSession($result->content);

                    $this->cookies->set('remember-me', true, time() + 15 * 86400);

             		return $this->response->redirect('admin');
             } else {
             	$this->flashSession->error('Invalid Credential');
        		$this->view->mode = "login";
             }
        }
    }

    private function _registerSession($user)
    {
        // var_dump($user);die;
        $this->session->set('auth', array('id' => $user->id, 'name' => $user->name, 'isSuperAdmin'=> $user->isSuperAdmin, 'access'=> serialize($user->access),'trustedLanguages'=> serialize($user->trustedLanguages),'picture'=>$user->picture));
    }


    // Method: POST, PUT, GET etc
	// Data: array("param" => "value") ==> index.php?param=value

	private function _callAPI($method, $url, $data = false)
	{
	    $curl = curl_init();

	    switch ($method)
	    {
	        case "POST":
	            curl_setopt($curl, CURLOPT_POST, 1);

	            if ($data)
	                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	            break;
	        case "PUT":
	            curl_setopt($curl, CURLOPT_PUT, 1);
	            break;
	        default:
	            if ($data)
	                $url = sprintf("%s?%s", $url, http_build_query($data));
	    }

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	    $result = curl_exec($curl);

	    curl_close($curl);

	    return $result;
	}
}
