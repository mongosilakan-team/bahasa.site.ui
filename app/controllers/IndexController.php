<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        $this->view->mode = 'public';
        $this->view->baseUrlApi = $this->config->application->baseUrlApi;
        $this->view->baseUrl = $this->config->application->baseUri;
        $auth = $this->session->get('auth');
        $this->view->signedIn = false;
        if ($auth) {
            $this->view->signedIn = true;
            $this->view->userName = $auth['name'];
            $this->view->userId = $auth['id'];
            $this->view->userPicture = $auth['picture'];
        }
    }
}
