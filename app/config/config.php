<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => '',
        'host'        => '',
        'username'    => '',
        'password'    => '',
        'dbname'      => '',
        'charset'     => '',
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => '/bitbucket/bahasa.site.ui/',
        'baseUrlApi'     => 'http://localhost/bitbucket/bahasa.site.api/'
    )
));
