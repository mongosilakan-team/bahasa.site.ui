<!DOCTYPE html>
<html lang="id" data-ng-app="app">

<head>
	{{ partial("frontend/partials/metadata") }}

	<title>Translator Jawa | Mongosilakan</title>

	<!-- Bootstrap Core CSS -->
	<!-- {{ stylesheet_link('libraries/bootstrap/css/bootstrap.min.css') }} -->
	{{ stylesheet_link('templates/creative/css/bootstrap.min.css') }}

	<!-- Custom Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'> {{ stylesheet_link('templates/creative/font-awesome/css/font-awesome.min.css') }}
	<!-- Plugin CSS -->
	{{ stylesheet_link('templates/creative/css/animate.min.css') }}
	<!-- Custom CSS -->
    {{ stylesheet_link('templates/creative/css/creative.css') }}
    <!-- Custom animate CSS -->
    {{ stylesheet_link('templates/creative/css/animate-custom.css') }}
    <!-- Custom CSS -->
	{{ stylesheet_link('templates/creative/css/custom-style.css') }}

	<!-- Loading bar -->
	{{ stylesheet_link('plugins/loading-bar/loading-bar.min.css') }}
	{{ stylesheet_link('plugins/select2/select2.css') }}
	<!-- Hightlight text area -->
	{{ stylesheet_link('plugins/jquery.highlighttextarea/jquery.highlighttextarea.min.css') }}


    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">

	<!-- Toastr -->
	{{ stylesheet_link('plugins/toastr/toastr.css') }}

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top">

	<!-- Navigations -->
	{{ partial("frontend/partials/navigation") }}

    <div style="width: inherit;height: inherit;" ng-class="animation">
    	<ng-view class="view"></ng-view>
    </div>

	{#{ partial("frontend/partials/ads") }#}


	<!-- jQuery -->
	{{ javascript_include('templates/creative/js/jquery.js') }}
	<!-- Bootstrap Core JavaScript -->
	{{ javascript_include('templates/creative/js/bootstrap.min.js') }}
	<!-- Plugin JavaScript -->
    <!-- jQuery easing -->
	{{ javascript_include('templates/creative/js/jquery.easing.min.js') }}
    <!-- jQuery Fittext -->
    {{ javascript_include('templates/creative/js/jquery.fittext.js') }}
    <!-- Wow js -->
    {{ javascript_include('templates/creative/js/wow.min.js') }}
	<!-- Custom Theme JavaScript -->
	{{ javascript_include('templates/creative/js/creative.js') }}
	<!-- ./wrapper -->
	{{ javascript_include('plugins/jQuery/jQuery-2.1.4.min.js') }}
	<!-- iCheck -->
	{{ javascript_include('plugins/iCheck/icheck.min.js') }}
	<!-- jQuery UI 1.11.2 -->
	{{ javascript_include('plugins/jQueryUI/jquery-ui.min.js') }}
	<!-- Autogrow -->
	{{ javascript_include('plugins/autogrow/autogrow.min.js') }}
	<!-- AngularJS -->
	{{ javascript_include('libraries/angular-1.5/angular.js')}}
    {{ javascript_include('libraries/angular-1.5/angular-route.min.js')}}
	{{ javascript_include('libraries/angular-1.5/angular-animate.min.js')}}
	{{ javascript_include('libraries/angular-1.5/angular-sanitize.min.js')}}
    {{ javascript_include('libraries/angular-1.5/angular-touch.min.js')}}
	{{ javascript_include('plugins/ng-table/ng-table.js')}}
    {{ javascript_include('libraries/angular-ui-bootstrap/ui-bootstrap.min.js')}}
    {{ javascript_include('app/frontend/app.js')}}
    {{ javascript_include('app/frontend/base-scripts/constants.js')}}
    {{ javascript_include('app/frontend/directives/commonDirectives.js')}}
	{{ javascript_include('app/frontend/modules/applicationModule.js')}}
	<script type="text/javascript">
		app.constant("config", {
			"base_api_url": "{{baseUrlApi}}"
		});

		applicationModule.constant( 'templateBaseUrl', '{{baseUrl}}' );

		{% if signedIn %}
		app.run(function(baseCommon) {
			baseCommon.userSignedIn({{userId}},'{{userName}}','{{userPicture}}');
		});
		{% endif %}
		function onSignIn(googleUser) {
			var googleService = angular.element(document.querySelector('[data-ng-app]')).injector().get('googleService');
			// console.log(googleUser.getAuthResponse().id_token);
			googleService.signIn(googleUser.getAuthResponse().id_token);
		}
	</script>

	{{ javascript_include('app/frontend/base-scripts/base-common.js')}}
    {{ javascript_include('app/frontend/services/baseService.js')}}
	{{ javascript_include('app/frontend/services/homepageService.js')}}
    {{ javascript_include('app/frontend/controllers/homepageController.js')}}
    {{ javascript_include('app/frontend/controllers/translateController.js')}}
    {{ javascript_include('app/frontend/services/translateService.js')}}
    {{ javascript_include('app/shared/services/resourceService.js')}}
    {{ javascript_include('app/shared/services/languageService.js')}}

    {{ javascript_include('app/shared/services/googleService.js')}}
    <!-- Loading bar -->
    {{ javascript_include('plugins/loading-bar/loading-bar.min.js') }}
    <!-- Toastr -->
	{{ javascript_include('plugins/toastr/toastr.min.js') }}
	<!-- Autosize input -->
    {{ javascript_include('plugins/autosize-input/autosize-input.js') }}
	<!-- Hightlight text area -->
	{{ javascript_include('plugins/jquery.highlighttextarea/jquery.highlighttextarea.min.js') }}
    {{ javascript_include('plugins/select2/select2.js') }}
	<!-- Angulartics -->
	{{ javascript_include('plugins/angulartics/angulartics.min.js') }}
    {{ javascript_include('plugins/angulartics-google-analytics/dist/angulartics-google-analytics.min.js') }}

	<script type="text/javascript">
	(function(i, s, o, g, r, a, m) {
	    i['GoogleAnalyticsObject'] = r;
	    i[r] = i[r] || function() {
	        (i[r].q = i[r].q || []).push(arguments)
	    }, i[r].l = 1 * new Date();
	    a = s.createElement(o),
	        m = s.getElementsByTagName(o)[0];
	    a.async = 1;
	    a.src = g;
	    m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-47155416-1', 'mongosilakan.net');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>

</body>

</html>
