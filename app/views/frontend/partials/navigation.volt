<nav id="mainNav" class="navbar navbar-default navbar-fixed-top" ng-class="navClass">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand page-scroll" href="" alt="Release Candidate">Mongosilakan v.2.0 (RC)</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a><div class="fb-like" data-href="https://www.facebook.com/pages/Translator-Jawa/144855132306335" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div></a>
				</li>
				<li>
					<a class="page-scroll" href="http://mongosilakan.net/translatorjawabeta">{{"{{MenuResources.OlderVersion}}"}}</a>
				</li>
				<li ng-show="isSignedIn" class="dropdown user user-menu ng-hide">
					<a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="">{{"{{userName}}"}}</span>
						<img src="{{" {{userPic}} "}}" class="user-image" alt="User Image">
					</a>
				</li>
				<li ng-if="isSignedIn">
					<a class="page-scroll" href="" ng-click="logout()"><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i></a>
				</li>
				<li ng-if="!isSignedIn" class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="">{{"{{CommonResources.SignIn}}"}}
				    <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="googleSignIn">
							<div class="g-signin2" data-onsuccess="onSignIn" data-longtitle="true" data-theme="dark" data-width="250" data-height="50"></div>
						</li>
					</ul>
				</li>

				<!-- <li>
					<div id="gConnect">
					  <div id="signin-button"></div>
					</div>
				</li> -->
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
