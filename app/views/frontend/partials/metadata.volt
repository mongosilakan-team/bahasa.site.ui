	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mongosilakan Team">
	<meta name="google-signin-client_id" content="304075802085-i4sqk4pdvmmegdejo2eqf381d6b6f25j.apps.googleusercontent.com"></meta>
	<link rel="shortcut icon" href="http://mongosilakan.net/translatorjawabeta/resource/images/favicon.ico" type="image/x-icon">
	<meta name="keyword" content="unggah-ungguh,translate bahasa daerah, kamus bahasa jawa, javanese, krama, ngoko, krama inggil, jawa, translate jawa,translator basa jawa, basa jawa, unggah-ungguh basa jawa, translator indonesia jawa, java translator, javanese translator, indonesian javanese translator, kamus bahasa jawa">
	<meta name="description" content="Layanan terjemahan online bahasa indonesia ke bahasa jawa dan sebaliknya dengan unggah-unguh bahasa jawa. Bahasa yang didukung: Bahasa Indonesia, Basa Ngoko, Basa Krama, dan Basa Krama Inggil">
	<meta name="country" content="Indonesia">
	<meta name="organization-Email" content="soetedja@gmail.com">
	<meta name="copyright" content="copyright 2016 - Irfan Soetedja @mongosilakan.net">
	<meta name="coverage" content="Worldwide">
	<meta name="revisit_after" content="30days">
	<meta name="identifier" content="http://www.mongosilakan.net/">
	<meta name="language" content="Indonesia, Jawa">
	<meta name="robots" content="follow">
	<meta name="googlebot" content="index, follow">
