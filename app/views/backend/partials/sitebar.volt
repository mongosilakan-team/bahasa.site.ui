<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <form ng-submit="shortcut()" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" ng-model="shortcutCommand" class="form-control" placeholder="Shortcut..">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-rocket"></i></button>
              </span>
            </div>
        </form>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul id="sitebar" class="sidebar-menu">
            <li class="header">{{"{{MenuResources.MainNavigation}}"}}</li>
            <li ng-repeat="nav in navigation" ng-click="sitebarAction($event)" class="{{" {{nav.status}} "}} treeview">
                <a href="#/{{"{{nav.url}}"}}">
                    <i class="fa {{" {{nav.icon}} "}}"></i>
                    <span ng-if="MenuResources[nav.label] != null">{{"{{MenuResources[nav.label]}}"}}</span>
                    <span ng-if="MenuResources[nav.label] == null">{{"{{nav.label}}"}}</span>
                    <i ng-if="{{" nav.hasSubs"}}" class="fa fa-angle-left pull-right"></i>
                </a>
                <ul ng-if="{{" nav.hasSubs "}}" class="treeview-menu">
                    <li ng-click="sitebarAction($event, true)" ng-repeat="subnav in nav.subs" class="{{"{{subnav.status}}"}}">
                        <a href="#/{{"{{subnav.url}}"}}">
                            <i class="fa {{" {{subnav.icon}} "}}"></i> 
                            <span ng-if="MenuResources[subnav.label] != null">{{"{{MenuResources[subnav.label]}}"}}</span>
                            <span ng-if="MenuResources[subnav.label] == null">{{"{{subnav.label}}"}}</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
