<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <style type="text/css">
	    body {
		  padding-top: 40px;
		  padding-bottom: 40px;
		  background-color: #eee;
		}

		.form-signin {
		  max-width: 330px;
		  padding: 15px;
		  margin: 0 auto;
		}
		.form-signin .form-signin-heading,
		.form-signin .checkbox {
		  margin-bottom: 10px;
		}
		.form-signin .checkbox {
		  font-weight: normal;
		}
		.form-signin .form-control {
		  position: relative;
		  height: auto;
		  -webkit-box-sizing: border-box;
		     -moz-box-sizing: border-box;
		          box-sizing: border-box;
		  padding: 10px;
		  font-size: 16px;
		}
		.form-signin .form-control:focus {
		  z-index: 2;
		}
		.form-signin input[type="email"] {
		  margin-bottom: -1px;
		  border-bottom-right-radius: 0;
		  border-bottom-left-radius: 0;
		}
		.form-signin input[type="password"] {
		  margin-bottom: 10px;
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		}
		.errorMessage {
			color:#d73925;
		}
	</style>

    <title>Mongosilakan - Login</title>

    <!-- bootstrap -->
    {{ stylesheet_link('libraries/bootstrap/css/bootstrap.min.css') }}

    <link href="signin.css" rel="stylesheet">
  </head>

  <body cz-shortcut-listen="true">

    <div class="container">
      {{ form('admin/start', 'method': 'post', 'class' : 'form-signin') }}

	        <h2 class="form-signin-heading">Please sign in</h2>
 			<h5>{{ flashSession.output() }}</h5>
	        <div>
	            <label class="sr-only" for="email">Username/Email</label>
	            <div>
	                {{ text_field('username', 'class' : 'form-control', 'placeholder' : 'Username/Email') }}
	            </div>
	        </div>
	        <div>
	            <label class="sr-only" for="password">Password</label>
	            <div>
	                {{ password_field('password', 'class' : 'form-control', 'placeholder' : 'Password') }}
	            </div>
	        </div>
	        <div class="checkbox">
		      <label>
		      	{{check_field('remember-me')}}Remember me
		      </label>
		    </div>
	        <div>
	            {{ submit_button('Sign in', 'class' : 'btn btn-lg btn-primary btn-block' ) }}
	        </div>
	</form>

    </div> <!-- /container -->

</body>
    <!-- ./wrapper -->
    {{ javascript_include('plugins/jQuery/jQuery-2.1.4.min.js') }}
    <!-- jQuery UI 1.11.2 -->
    {{ javascript_include('plugins/jQueryUI/jquery-ui.min.js') }}
    <!-- Bootstrap  -->
    {{ javascript_include('libraries/bootstrap/js/bootstrap.min.js') }}
</html>