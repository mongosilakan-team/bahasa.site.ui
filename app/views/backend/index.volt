<!DOCTYPE html>
<html lang="id" data-ng-app="app">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mongosilakan Website">
    <meta name="author" content="Mongosilakan Team">
    <link rel="icon"
      type="image/png"
      href="contents/favicon.ico">
    <title>Mongosilakan</title>
    <!-- bootstrap -->
    {{ stylesheet_link('libraries/bootstrap/css/bootstrap.min.css') }}
    <!-- font awesome-->
    {{ stylesheet_link('contents/fonts/font-awesome-4.3.0/css/font-awesome.css') }}

    <!-- Ionicons 2.0.0 -->
    <!-- <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    -->
    <!-- Admin LTE template -->
    {{ stylesheet_link('templates/AdminLTE-2.1.1/css/AdminLTE.min.css') }} {{ stylesheet_link('templates/AdminLTE-2.1.1/css/skins/_all-skins.css') }}
    <!-- iCheck Plugin -->
    {{ stylesheet_link('plugins/iCheck/flat/blue.css') }}
    <!-- Morris Chart -->
    {{ stylesheet_link('plugins/morris/morris.css') }}
    <!-- JVectorMap -->
    {{ stylesheet_link('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}
    <!-- Datepicker -->
    {{ stylesheet_link('plugins/datepicker/datepicker3.css') }}
    <!-- Date range picker -->
    {{ stylesheet_link('plugins/daterangepicker/daterangepicker-bs3.css') }}
    <!-- Bootstrap wysihtml5 -->
    {{ stylesheet_link('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}

    {{ stylesheet_link('plugins/ng-table/ng-table.css') }}

    {{ stylesheet_link('templates/AdminLTE-2.1.1/css/custom-admin-style.css') }}

    {{ stylesheet_link('plugins/loading-bar/loading-bar.min.css') }}

    {{ stylesheet_link('plugins/toastr/toastr.css') }}
    <!-- iCheck -->
    {{ stylesheet_link('plugins/iCheck/minimal/blue.css') }}


</head>

<body class="skin-blue sidebar-mini fixed">
    <button type="button" id="clearToaster" ng-if="anyToaster" ng-click="clearToaster()" class="btn btn-sm btn-success" id="clearlasttoast" style="position: fixed;z-index: 99999999 !important;bottom: 16px;right: 26px;">Clear Message</button>
    <div class="wrapper">
        <!-- Header -->
        {{ partial("backend/partials/header") }}
        <!-- Sitebar -->
        {{ partial("backend/partials/sitebar") }}
        <!-- Content -->
        <!-- {{ content() }} -->
        <!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
		    <div ng-view></div>
		</div>
		<!-- /.content-wrapper -->

        <!-- Control sitebar -->
        {#{ partial("backend/partials/control-sitebar") }#}
        <!-- Footer -->
        {{ partial("backend/partials/footer") }}
        {{ hidden_field("baseUrlApi", "value": "ttest") }}
    </div>
    <!-- ./wrapper -->
    {{ javascript_include('plugins/jQuery/jQuery-2.1.4.min.js') }}
    <!-- iCheck -->
    {{ javascript_include('plugins/iCheck/icheck.min.js') }}
    <!-- jQuery UI 1.11.2 -->
    {{ javascript_include('plugins/jQueryUI/jquery-ui.min.js') }}
    <!-- Bootstrap  -->
    {{ javascript_include('libraries/bootstrap/js/bootstrap.min.js') }}
    <!-- AngularJS -->
    {{ javascript_include('libraries/angular/angular.min.js')}}
    {{ javascript_include('libraries/angular/angular-route.min.js')}}
    {{ javascript_include('libraries/angular/ui-bootstrap.js')}}
    {{ javascript_include('libraries/angular/angular-animate.js')}}
    {{ javascript_include('plugins/ng-table/ng-table.js')}}

    {{ javascript_include('app/backend/app.js')}}
    {{ javascript_include('app/backend/base-scripts/constants.js')}}
    {{ javascript_include('app/backend/directives/commonDirectives.js')}}

    <script type="text/javascript">
    app.constant("config", {
        "base_api_url": "{{baseUrlApi}}"
    });
    </script>

    {{ javascript_include('app/backend/base-scripts/base-common.js')}}
    {{ javascript_include('app/backend/services/baseService.js')}}
    {{ javascript_include('app/backend/services/resources/resourceService.js')}}

    <!-- Configuration Module -->
    {{ javascript_include('app/backend/modules/configurationsModule.js')}}
    {{ javascript_include('app/backend/services/configurations/userService.js')}}
    {{ javascript_include('app/backend/controllers/configurations/loginController.js')}}
    {{ javascript_include('app/backend/controllers/configurations/userManageController.js')}}
    {{ javascript_include('app/backend/controllers/configurations/userCreateController.js')}}
    {{ javascript_include('app/backend/controllers/configurations/userEditController.js')}}

    {{ javascript_include('app/backend/services/configurations/statusService.js')}}
    {{ javascript_include('app/backend/controllers/configurations/statusManageController.js')}}
    {{ javascript_include('app/backend/controllers/configurations/statusCreateController.js')}}
    {{ javascript_include('app/backend/controllers/configurations/statusEditController.js')}}

    <!-- Dictionary Module -->
    {{ javascript_include('app/backend/modules/dictionaryModule.js')}}

    {{ javascript_include('app/backend/services/dictionary/translationService.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/translationManageController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/translationCreateController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/translationEditController.js')}}

    {{ javascript_include('app/backend/services/dictionary/wordService.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/wordManageController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/wordCreateController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/wordEditController.js')}}

    {{ javascript_include('app/backend/services/dictionary/languageService.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/languageManageController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/languageCreateController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/languageEditController.js')}}

    {{ javascript_include('app/backend/services/dictionary/wordTypeService.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/wordTypeManageController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/wordTypeCreateController.js')}}
    {{ javascript_include('app/backend/controllers/dictionary/wordTypeEditController.js')}}

    <!-- Donation Module -->
    {{ javascript_include('app/backend/modules/donationModule.js')}}
    {{ javascript_include('app/backend/services/donation/donorService.js')}}
    {{ javascript_include('app/backend/controllers/donation/donorManageController.js')}}
    {{ javascript_include('app/backend/controllers/donation/donorCreateController.js')}}
    {{ javascript_include('app/backend/controllers/donation/donorEditController.js')}}

    {{ javascript_include('app/backend/services/donation/donationService.js')}}
    {{ javascript_include('app/backend/controllers/donation/donationManageController.js')}}
    {{ javascript_include('app/backend/controllers/donation/donationCreateController.js')}}
    {{ javascript_include('app/backend/controllers/donation/donationEditController.js')}}

    <!-- Run Module -->
    {{ javascript_include('app/backend/modules/runModule.js')}}
    {{ javascript_include('app/backend/services/run/runService.js')}}
    {{ javascript_include('app/backend/controllers/run/runController.js')}}

    <!-- Misc -->
    {{ javascript_include('app/backend/controllers/misc/dashboardCtrl.js')}}
    {{ javascript_include('app/backend/controllers/misc/aboutCtrl.js')}}
    {{ javascript_include('app/backend/controllers/misc/contactCtrl.js')}}
    {{ javascript_include('app/backend/controllers/misc/componentsCtrl.js')}}
    <!-- {{ javascript_include('js/utils.js') }} -->


    <!-- daterangepicker -->
    {{ javascript_include('plugins/moment/moment+locales.js') }} {{ javascript_include('plugins/daterangepicker/daterangepicker.js') }}
    <!-- datepicker -->
    {{ javascript_include('plugins/datepicker/bootstrap-datepicker.js') }}
    <!-- Bootstrap WYSIHTML5 -->
    {{ javascript_include('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
    <!-- Slimscroll -->
    {{ javascript_include('plugins/slimScroll/jquery.slimscroll.min.js') }}
    <!-- FastClick -->
    {{ javascript_include('plugins/fastclick/fastclick.min.js') }}
    <!-- AdminLTE App -->
    {{ javascript_include('templates/AdminLTE-2.1.1/js/app.js') }}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    {{ javascript_include('templates/AdminLTE-2.1.1/js/pages/dashboard.js') }}
    <!-- AdminLTE for demo purposes -->
    {{ javascript_include('templates/AdminLTE-2.1.1/js/demo.js') }}

    {{ javascript_include('plugins/loading-bar/loading-bar.min.js') }}

    {{ javascript_include('plugins/toastr/toastr.min.js') }}

       <!-- Morris.js charts -->

    <!-- jQuery 2.1.4 -->
    <!-- // <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> -->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
     <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>

</body>

</html>
