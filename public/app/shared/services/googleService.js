applicationModule.service('googleService', googleService);

function googleService($http, $q, $rootScope, config, constants, baseCommon) {
    var googleService = {
        signIn :_signIn,
        // signOut :_signOut,
    };
    // runServices = angular.extend(angular.copy(baseService), runServices);
    return googleService;

	function _signIn($entity) {
		$.post(config.base_api_url + "api/user/login", {data : $entity}, function(data) {
            baseCommon.signedIn(data.content.model);
            baseCommon.pushToasterMessages(data.content.messages);
		});
	}

    // function _signOut() {
    //     var defer = $q.defer();
    //     $http({
    //         method: 'GET',
    //         url: config.base_api_url + "api/user/logout"
    //     }).success(function(data) {
    //         defer.resolve(data);
    //     }).error(function(data) {
    //         defer.reject(data);
    //     });
    //     return defer.promise;
    // }
    //
	// function _signInx($entity) {
	// 	console.log($entity);
	// 	console.log(config.base_api_url + "api/translate/translate");
    //     var defer = $q.defer();
    //     $http({
    //         method: 'POST',
    //         url: config.base_api_url + "api/translate/translate",
    //         data: $.param($entity),
    //         headers: {
    //             'Content-Type': 'application/x-www-form-urlencoded'
    //         }
    //     }).success(function(data) {
	// 		console.log("data")
    //     }).error(function(data) {
    //     });
    //     return defer.promise;
    // }
}
