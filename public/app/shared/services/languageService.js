app.service('languageService', languageServices);

function languageServices($http, $q, config, constants, baseService) {
    var languageServices = {
        getApi: _getApi,
        getActiveAndPending: _getActiveAndPending,
        getAllUserLanguages: _getAllUserLanguages,
    }
    languageServices = angular.extend(angular.copy(baseService), languageServices);
    return languageServices;

    function _getApi() {
        return constants.modules.dictionary.language.api;
    }

    function _getActiveAndPending() {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: config.base_api_url + this.getApi() + 'getActiveAndPending'
        }).success(function(data) {
            defer.resolve(data);
        }).error(function(data) {
            defer.reject(data);
        });
        return defer.promise;
    }

    function _getAllUserLanguages() {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: config.base_api_url + this.getApi() + 'getUserLanguages'
        }).success(function(data) {
            defer.resolve(data);
        }).error(function(data) {
            defer.reject(data);
        });
        return defer.promise;
    }
}
