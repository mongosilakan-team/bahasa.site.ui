runModule.service('runService', runServices);

function runServices($http, $q, config, constants, baseService) {
    var runServices = {
        getApi: _getApi,
        translate: _translate,
    };
    runServices = angular.extend(angular.copy(baseService), runServices);
    return runServices;

    function _getApi(){
    	return constants.modules.translate.translate.api;
    }

    function _translate($from, $to, $source) {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: config.base_api_url + _getApi() + "translate" + "?from="+ $from +" &to="+ $to +" &source=" + escape($source)
        }).success(function(data) {
        	if (data.content === undefined) {
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            defer.reject(data);
        });
        return defer.promise;
    }
}
