dictionaryModule.service('translationService', translationServices);

function translationServices($http, $q, config, constants, baseService) {
    var translationServices = {
        getApi: _getApi,
    };
    translationServices = angular.extend(angular.copy(baseService), translationServices);
    return translationServices;

    function _getApi() {
        return constants.modules.dictionary.translation.api;
    }
}