donationModule.service('donorService', donorServices);

function donorServices($http, $q, config, constants, baseService) {
    var donorServices = {
        getApi: _getApi,
    };
    donorServices = angular.extend(angular.copy(baseService), donorServices);
    return donorServices;

    function _getApi(){
    	return constants.modules.donation.donor.api;
    }
}