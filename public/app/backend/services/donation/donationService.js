donationModule.service('donationService', donationServices);

function donationServices($http, $q, config, constants, baseService) {
    var donationServices = {
        getApi: _getApi,
    };
    donationServices = angular.extend(angular.copy(baseService), donationServices);
    return donationServices;

    function _getApi(){
    	return constants.modules.donation.donation.api;
    }
}