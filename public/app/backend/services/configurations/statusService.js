configurationsModule.service('statusService', statusServices);

function statusServices($http, $q, config, constants, baseService) {
    var statusServices = {
        getApi :_getApi,   
    	//getAll : _getAll,
    };

    statusServices = angular.extend(angular.copy(baseService), statusServices);

    return statusServices;

    function _getApi(){
        return constants.modules.configurations.status.api;
    }

    // function _getAll() {
    //     var defer = $q.defer();
    //     $http({
    //         method: 'GET',
    //         url: config.base_api_url + api
    //     }).success(function(data) {
    //         defer.resolve(data);
    //     }).error(function(data) {
    //         defer.reject(data);
    //     });
    //     return defer.promise;
    // }
}
