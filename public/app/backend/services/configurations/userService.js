configurationsModule.service('userService', userServices);

function userServices($http, $q, config, constants, baseService) {
    var userServices = {
        // login: _login,
        getApi: _getApi,
        logout: _logout,
    };
    userServices = angular.extend(angular.copy(baseService), userServices);
    return userServices;
    // function _login($entity) {
    //     var defer = $q.defer();
    //     $http({
    //         method: 'POST',
    //         url: config.base_api_url + constants.module.configurations.user.api + "login",
    //         data: $.param($entity),
    //         headers: {
    //             'Content-Type': 'application/x-www-form-urlencoded'
    //         }
    //     }).success(function(data) {
    //         defer.resolve(data);
    //     }).error(function(data) {
    //         defer.reject(data);
    //     });
    //     return defer.promise;
    // }
    function _getApi() {
        return constants.modules.configurations.user.api;
    }

    function _logout() {
        var defer = $q.defer();
        $http({
            method: 'GET',
            url: config.base_api_url + _getApi() + "logout"
        }).success(function(data) {
            defer.resolve(data);
        }).error(function(data) {
            defer.reject(data);
        });
        return defer.promise;
    }
}