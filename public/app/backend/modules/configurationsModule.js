var configurationsModule = angular.module('configurationsModule', []);

configurationsModule.config(function($routeProvider) {
    $routeProvider.when("/configurations/users", {
            templateUrl: "app/backend/views/configurations/user-manage.html",
            controller: "userManageController"
        })
        .when('/configurations/user/create', {
            templateUrl: 'app/backend/views/configurations/user-create.html',
            controller: 'userCreateController'
        })
        .when('/configurations/user/:id', {
            templateUrl: 'app/backend/views/configurations/user-edit.html',
            controller: 'userEditController'
        })
        .when("/configurations/statuses", {
            templateUrl: "app/backend/views/configurations/status-manage.html",
            controller: "statusManageController"
        })
        .when('/configurations/status/create', {
            templateUrl: 'app/backend/views/configurations/status-create.html',
            controller: 'statusCreateController'
        })
        .when('/configurations/status/:id', {
            templateUrl: 'app/backend/views/configurations/status-edit.html',
            controller: 'statusEditController'
        });
});
