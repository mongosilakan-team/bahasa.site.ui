var runModule = angular.module('runModule', []);

runModule.config(function($routeProvider) {
    $routeProvider.when("/run", {
            templateUrl: "app/backend/views/run/run.html",
            controller: "runController"
        })
});
