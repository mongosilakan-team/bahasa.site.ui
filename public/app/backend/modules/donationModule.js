var donationModule = angular.module('donationModule', []);

donationModule.config(function($routeProvider) {
    $routeProvider.when("/donation/donors", {
            templateUrl: "app/backend/views/donation/donor-manage.html",
            controller: "donorManageController"
        })
        .when('/donation/donor/create', {
            templateUrl: 'app/backend/views/donation/donor-create.html',
            controller: 'donorCreateController'
        })
        .when('/donation/donor/:id', {
            templateUrl: 'app/backend/views/donation/donor-edit.html',
            controller: 'donorEditController'
        })
        .when("/donation/donations", {
            templateUrl: "app/backend/views/donation/donation-manage.html",
            controller: "donationManageController"
        })
        .when('/donation/donation/create', {
            templateUrl: 'app/backend/views/donation/donation-create.html',
            controller: 'donationCreateController'
        })
        .when('/donation/donation/:id', {
            templateUrl: 'app/backend/views/donation/donation-edit.html',
            controller: 'donationEditController'
        });
});
