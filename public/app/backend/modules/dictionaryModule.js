var dictionaryModule = angular.module('dictionaryModule', []);

dictionaryModule.config(function($routeProvider) {
    $routeProvider.when("/dictionary/words", {
            templateUrl: "app/backend/views/dictionary/word-manage.html",
            controller: "wordManageController"
        })
        .when('/dictionary/word/create', {
            templateUrl: 'app/backend/views/dictionary/word-create.html',
            controller: 'wordCreateController'
        })
        .when('/dictionary/word/:id', {
            templateUrl: 'app/backend/views/dictionary/word-edit.html',
            controller: 'wordEditController'
        })
        .when("/dictionary/languages", {
            templateUrl: "app/backend/views/dictionary/language-manage.html",
            controller: "languageManageController"
        })
        .when('/dictionary/language/create', {
            templateUrl: 'app/backend/views/dictionary/language-create.html',
            controller: 'languageCreateController'
        })
        .when('/dictionary/language/:id', {
            templateUrl: 'app/backend/views/dictionary/language-edit.html',
            controller: 'languageEditController'
        })
        .when("/dictionary/wordTypes", {
            templateUrl: "app/backend/views/dictionary/word-type-manage.html",
            controller: "wordTypeManageController"
        })
        .when('/dictionary/wordType/create', {
            templateUrl: 'app/backend/views/dictionary/word-type-create.html',
            controller: 'wordTypeCreateController'
        })
        .when('/dictionary/wordType/:id', {
            templateUrl: 'app/backend/views/dictionary/word-type-edit.html',
            controller: 'wordTypeEditController'
        })
        .when("/dictionary/translations", {
            templateUrl: "app/backend/views/dictionary/translation-manage.html",
            controller: "translationManageController"
        })
        .when('/dictionary/translation/create', {
            templateUrl: 'app/backend/views/dictionary/translation-create.html',
            controller: 'translationCreateController'
        })
        .when('/dictionary/translation/:id', {
            templateUrl: 'app/backend/views/dictionary/translation-edit.html',
            controller: 'translationEditController'
        });
});
