donationModule.controller('donorCreateController', donorCreateController);

function donorCreateController($scope, $location, baseCommon, donorService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            donorService.create($scope.donor).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.donation.subs.donors.url);
            }, function() {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}