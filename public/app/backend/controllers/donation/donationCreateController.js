donationModule.controller('donationCreateController', donationCreateController);

function donationCreateController($scope, $location, baseCommon, donationService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            donationService.create($scope.donation).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.donation.subs.donations.url);
            }, function() {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}