donationModule.controller('donorEditController', donorEditController);

function donorEditController($scope, $routeParams, $location, baseCommon, donorService) {
    baseCommon.clearMessages();
    donorService.getById($routeParams.id).then(function(result) {
        $scope.donor = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            donorService.update($scope.donor).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.donation.subs.donors.url);
            }, function(result) {
                baseCommon.pushMessages(result);
            }).finally(function() {});
        }
    };
}