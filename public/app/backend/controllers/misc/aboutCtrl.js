app.controller('aboutCtrl', ['$scope', function($scope)
{
	$scope.message = "About";
	console.log("controller");
	$scope.alerts = [
    { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
    { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
  ];

  $scope.addAlert = function() {
    $scope.alerts.push({msg: 'Another alert!', icon});
  };

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
}])