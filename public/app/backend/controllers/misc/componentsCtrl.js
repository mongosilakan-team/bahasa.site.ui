app.controller('componentsCtrl', ['$scope', function($scope) {

    /// Alerts
    $scope.alerts = [{
        type: 'danger',
        icon: 'ban',
        msg: 'Oh snap! Change a few things up and try submitting again.'
    }, {
        type: 'success',
        icon: 'check',
        msg: 'Well done! You successfully read this important alert message.'
    }, {
        type: 'info',
        icon: 'info',
        msg: 'Well done! You successfully read this important alert message.'
    }, {
        type: 'warning',
        icon: 'warning',
        msg: 'Well done! You successfully read this important alert message.'
    }];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.addAlert = function() {
        $scope.alerts.push({
            msg: 'Another alert!',
            type: 'info',
            icon: 'info'
        });
    };

    //// Accordions
    $scope.oneAtATime = true;

    $scope.groups = [{
        title: 'Dynamic Group Header - 1',
        content: 'Dynamic Group Body - 1'
    }, {
        title: 'Dynamic Group Header - 2',
        content: 'Dynamic Group Body - 2'
    }];

    //// Buttons
    $scope.singleModel = 1;

    $scope.radioModel = 'Middle';

    $scope.checkModel = {
        left: false,
        middle: true,
        right: false
    };

}])
