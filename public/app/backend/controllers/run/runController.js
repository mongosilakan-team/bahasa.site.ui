runModule.controller('runController', runController);

function runController($scope, $location, $timeout, baseCommon, runService) {
    baseCommon.clearMessages();
    //init var
    $scope.entity = {};
    $scope.entity.from = "id-ID";
    $scope.entity.to = "jv-NG";

    $scope.onChange = _onChange;
    var executionTime = 200; // in milisecond
    var countDown = executionTime;
    var isTimerStarted = false;

    function _onChange(){
        countDown = executionTime;
        if(!isTimerStarted){
            isTimerStarted = true;
            _startTimer();
        }
    }

    function _startTimer(){
        $timeout(function() {
            countDown--;
            if(!countDown){
                isTimerStarted = false;
                _executor();
            } else{
                _startTimer();
            }
        }, 1);
    }

    function _executor(){
        if($scope.entity.source === ''){ // if the source is empty no need to send request to server
            $scope.entity.result = '';
            return;
        } else{
            runService.translate($scope.entity.from, $scope.entity.to, $scope.entity.source).then(function(result) {
                $scope.entity.result = result.content.model;
            }, function(result) {
                baseCommon.pushMessages(result);
            }).finally(function() {});

        }
    }
}
