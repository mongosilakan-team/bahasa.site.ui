dictionaryModule.controller('translationManageController', translationManageController);

function translationManageController($scope, $timeout, $modal, baseCommon, translationService, ngTableParams) {
    baseCommon.clearMessages();
    baseCommon.clearErrorMessages();
    // set timeout for waiting resource to be ready
    $timeout(function() {
        baseCommon.setTitle($scope.DictionaryResources.Translations);
    }, 0);
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 25, // count per page
        sorting: {
            indonesian: 'asc' // initial sorting
        }
    }, {
        total: 0, // length of data
        getData: function($defer, params) {
            translationService.search(params.filter(), params.count(), params.sorting(), params.page()).then(function(result) {
                $timeout(function() {
                    params.total(result.content.model.total_items);
                    $defer.resolve(result.content.model.items);
                }, 0);
            }, function(error) {
                baseCommon.pushMessages(error.content.messages);
            });
        }
    });

    $scope.animationsEnabled = true;
    $scope.delete = function($id, $name) {
        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            template: '<div class="modal-header"><h3 class="modal-title"><i class="icon fa fa-warning"></i> ' + $scope.CommonResources.DeleteConfirmation + '</div>' + '<div class="modal-body"> ' + $scope.CommonResources.Msg_DeleteConfirmation.format($scope.DictionaryResources.Word.toLowerCase(), $name) + '</div> ' + '<div class = "modal-footer" >' + '<button class = "btn btn-primary" ng-click = "ok()" > OK </button> ' + '<button class = "btn btn-default" ng-click = "cancel()" > Cancel </button> </div>',
            controller: 'translationDeleteConfirmationCtrl',
            size: 'md',
            resolve: {
                id: function() {
                    return $id;
                }
            }
        });
        modalInstance.result.then(function(result) {
            $scope.tableParams.reload();
        }, function() {}).finally(function() {
            baseCommon.forceCloseModal();
        });
    };
    $scope.toggleAnimation = function() {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };
}
dictionaryModule.controller('translationDeleteConfirmationCtrl', translationDeleteConfirmationCtrl);

function translationDeleteConfirmationCtrl($scope, $modalInstance, baseCommon, id, translationService, constants) {
    $scope.ok = function() {
        translationService.delete(id).then(function(result) {
            baseCommon.pushMessages(result.content.messages);
        }, function(result) {
            baseCommon.pushMessages(result.content.messages);
        }).finally(function() {
            $modalInstance.close(id);
        });
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}