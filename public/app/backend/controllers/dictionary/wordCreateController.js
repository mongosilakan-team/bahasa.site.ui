dictionaryModule.controller('wordCreateController', wordCreateController);

function wordCreateController($scope, $routeParams, $location, baseCommon, wordService, wordTypeService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            $scope.entity.word_type_id = null;
            wordService.create($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.words.url);
            }, function(result) {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };

    wordTypeService.getAll().then(function(result) {
        $scope.wordTypeOptions = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
}