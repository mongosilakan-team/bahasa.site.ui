dictionaryModule.controller('languageCreateController', languageCreateController);

function languageCreateController($scope, $location, baseCommon, languageService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        if (isValid) {
            languageService.create($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.languages.url);
            }, function() {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}