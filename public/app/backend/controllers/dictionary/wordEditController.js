dictionaryModule.controller('wordEditController', wordEditController);

function wordEditController($scope, $routeParams, $location, baseCommon, wordService, wordTypeService) {
    baseCommon.clearMessages();
    wordService.getById($routeParams.id).then(function(result) {
        $scope.entity = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    wordTypeService.getAll().then(function(result) {
        $scope.wordTypeOptions = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            wordService.update($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.words.url);
            }, function(result) {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}