configurationsModule.controller('statusEditController', statusEditController);

function statusEditController($scope, $routeParams, $location, baseCommon, statusService) {
    baseCommon.clearMessages();
    statusService.getById($routeParams.id).then(function(result) {
        $scope.entity = result.content.model;
    }, function(result) {
        baseCommon.pushMessages(result);
    });

    $scope.submitForm = function(isValid) {
        if (isValid) {
            statusService.update($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                    $location.path($scope.navigation.configurations.subs.statuses.url);
                
            }, function(error) {
                baseCommon.pushMessages(error.content.messages);
            }).finally(function() {});
        }
    };
}
