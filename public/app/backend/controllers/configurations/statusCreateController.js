configurationsModule.controller('statusCreateController', statusCreateController);

function statusCreateController($scope, $location, baseCommon, statusService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        if (isValid) {
            statusService.create($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.configurations.subs.statuses.url);
            }, function() {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}