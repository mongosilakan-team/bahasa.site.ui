app.constant("constants", {
    "modules": {
        resources: {
            resource: {
                api: "api/resource/"
            }
        },
        configurations: {
            user: {
                api: "api/user/"
            },
            status: {
                api: "api/status/"
            }
        },
        dictionary: {
            word: {
                api: "api/word/"
            },
            language: {
                api: "api/language/"
            },
            wordType: {
                api: "api/wordType/"
            },
            translation: {
                api:"api/translation/"
            }
        },
        donation: {
            donor: {
                api: "api/donor/"
            },
            donation: {
                api: "api/donation/"
            }
        },
        run: {
            run: {
                api: "api/run/"
            },
        },
        translate: {
            translate: {
                api: "api/translate/"
            },
        }
    },
    "message_type": {
        success: 'success',
        warning: 'warning',
        error: 'error',
        info: 'info'
    },
    "status": {
        active: '1',
        inactive: '2',
        pending: '3',
        review: '4'
    }
});
