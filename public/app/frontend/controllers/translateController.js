applicationModule.controller('translateController', translateController);

function translateController($http, $scope, $filter, $rootScope, $uibModal, $location, $timeout, baseCommon, translateService) {
    baseCommon.clearMessages();

    $scope.entity = {};
    $rootScope.$on('resourceLoaded', function(event, data) {
        baseCommon.setTitle($scope.MenuResources.TranslatoJawaV2);
    });

    var renderDropdownDictionary = function(){
        $scope.dictionaryLanguages = angular.copy($rootScope.activeAndPendingLanguageList);
        $scope.dictionaryLanguages.unshift({
            id: 0,
            name: $scope.CommonResources.All
        });
        $scope.entity.dictionary_lang = $scope.dictionaryLanguages[0];
    }

    $rootScope.$on('activeAndPendingLanguageLoaded', function(event, data) {
        renderDropdownDictionary();
    });

    if($rootScope.activeAndPendingLanguageList){
        $timeout(function() {
            renderDropdownDictionary();
        }, 1000);
    }

    var source = $('#source');
    source.autogrow({
        onInitialize: true
    });
    //init var
    $scope.entity.from = angular.copy($rootScope.from);
    $scope.entity.to = angular.copy($rootScope.to);

    source.val("");
    $scope.entity.source = source.val();

    _executor();
    $scope.onChange = _onChange;
    var executionTime = 200; // in milisecond
    var countDown = executionTime;
    var isTimerStarted = false;

    // $scope.$watch('entity.source', function(value, oldValue) {
    //     console.log(value, oldValue);
    //     if (value === "" || value == oldValue) {
    //         $scope.entity.result = '';
    //         translateService.cancel(lastRequest);
    //         return;
    //     }
    //     countDown = executionTime;
    //     if (!isTimerStarted) {
    //         isTimerStarted = true;
    //         _startTimer();
    //     }
    // });

    $rootScope.$on('TRANSLATE', function() {
        if ($scope.entity.source === "") {
            $scope.entity.result = '';
            translateService.cancel(lastRequest);
            return;
        }
        countDown = executionTime;
        if (!isTimerStarted) {
            isTimerStarted = true;
            _startTimer();
        }
    });

    function _onChange(o, $event) {
        $scope.entity.source = $('textarea').val();
            if ($scope.entity.source === "" ) {
                $scope.entity.result = '';
                translateService.cancel(lastRequest);
                return;
            }
            countDown = executionTime;
            if (!isTimerStarted) {
                isTimerStarted = true;
                _startTimer();
            }
    }

    $scope.selectWord = function(model) {
        console.log(model);
    };

    function _startTimer() {
        $timeout(function() {
            countDown--;
            if (!countDown) {
                isTimerStarted = false;
                _executor();
            } else {
                _startTimer();
            }
        }, 1);
    }

    $scope.$on(
        "$destroy",
        function handleDestroyEvent() {
            translateService.cancel(lastRequest);
        }
    );

    var lastRequest = _executor();

    function _executor() {
        $('textarea').val($('textarea').val().replace(/ +(?= )/g, ''));
        translateService.cancel(lastRequest);
        var $from = angular.copy($rootScope.from);
        var $to = angular.copy($rootScope.to);
        var entity = {
            from:angular.copy($rootScope.from),
            to:angular.copy($rootScope.to),
            source:angular.copy($scope.entity.source)
        }
        lastRequest = translateService.translate(entity);
        lastRequest.then(
            function handleTranslateResolve(result) {
                if (result.content === undefined) {
                    baseCommon.pushMessages(result);
                } else {
                    $scope.entity.result = result.content.model;
                }
            },
            function handleTranslateReject(result) {
                if (!result.config) {
                    baseCommon.pushMessages(result);
                }
            }
        );
        return (lastRequest);
    }

    $scope.getWordDictionaryDetails = function($item, $model, $label, $event) {
        translateService.getWordDictionaryDetails($item.id).then(function(result) {
            // console.log(result.content.model);
            result.content.model.member.unshift({
                result_language_id: $item.language_id,
                result: $item.word,
                id: $item.id
            });
            $scope.wordDetails = result.content.model.member;
        }, function(error) {});
    }

    $scope.getSuggestionWordDictionary = function(val, $isGet) {
        return translateService.getSuggestionWordDictionary(val, $scope.entity.dictionary_lang.id).then(function(result) {
            return result.content.model;
        }, function(error) {});
    };

    var indexedTeams = [];

    $scope.wordsToFilter = function() {
        indexedTeams = [];
        return $scope.wordDetails;
    }

    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.result_language_id) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.result_language_id);
        }
        return teamIsNew;
    }
}
