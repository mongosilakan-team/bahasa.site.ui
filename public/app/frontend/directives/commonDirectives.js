app.directive('messagesContainer', function() {
    return {
        template: '<div class="row messages-container">' + '<div ng-repeat="message in messages" class="col-md-12 messages">' + '<div class="alert alert-{{message.type}} alert-dismissable">' + '<button type="button" ng-click="messages.splice($index,1)" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + '<i class="icon fa fa-{{message.icon}}"></i> {{message.message}}' + '</div>' + '</div>' + '</div>'
    };
});

app.directive('script', function() {
    return {
        restrict: 'E',
        scope: false,
        link: function(scope, elem, attr) {
            if (attr.type === 'text/javascript-lazy') {
                var code = elem.text();
                var f = new Function(code);
                f();
            }
        }
    };
});

app.directive('statusLabel', function($rootScope, $compile, constants) {
    var linker = function(scope, element, attrs) {
        var active = '<span class="label label-success">' + $rootScope.ConfigurationResources.Active + '</span>';
        var inactive = '<span class="label label-danger">' + $rootScope.ConfigurationResources.Inactive + '</span>';
        var pending = '<span class="label label-warning">' + $rootScope.ConfigurationResources.Pending + '</span>';
        var review = '<span class="label label-default">' + $rootScope.ConfigurationResources.Review + '</span>';
        var getTemplate = function(value) {
            var template = '';
            switch (value) {
                case constants.status.active:
                    template = active;
                    break;
                case constants.status.inactive:
                    template = inactive;
                    break;
                case constants.status.pending:
                    template = pending;
                    break;
                case constants.status.review:
                    template = review;
                    break;
            }
            return template;
        };
        element.html(getTemplate(scope.value)).show();
        $compile(element.contents())(scope);
    };
    return {
        restrict: 'A',
        link: linker,
        scope: {
            value: '=value'
        }
    };
});

app.directive("statusDropdown", function($timeout, $rootScope, $compile, resourceService, statusService) {
    var options = [];
    statusService.getAll().then(function(result) {
        angular.forEach(result.content.model, function(value, key) {
            value.name = $rootScope.ConfigurationResources[value.name] === undefined ? value.name : $rootScope.ConfigurationResources[value.name];
        });
        options = result.content.model;
    }, function() {
        console.log("Error Happened");
    });
    var linker = function(scope, element, attrs) {
        $timeout(function() {
            resourceService.commonResource().then(function(result) {
                var template = '<select ng-init="ngModel = ngModel" name="' + scope.name + '" id="' + name + '" class="form-control"';
                template += 'ng-model="ngModel" ng-required=' + scope.required + '>';
                angular.forEach(options, function(value, key) {
                    template += '<option value="' + value.id + '" >' + value.name + '</option>'
                });
                template += '</select>';
                element.html(template).show();
                $compile(element.contents())(scope);
            }, function(error) {
                baseCommon.pushMessages(error.content.messages);
            });
        }, 100);
    };
    return {
        restrict: 'E',
        link: linker,
        scope: {
            value: '=value',
            ngModel: '=ngModel',
            name: '@name',
            required: '@required'
        }
    };
});

app.directive("languageDropdown", function($timeout, $rootScope, $compile, resourceService, languageService) {
    var options = [];
    languageService.getAll().then(function(result) {
        angular.forEach(result.content.model, function(value, key) {
            value.name = $rootScope.DictionaryResources[value.name] === undefined ? value.name : $rootScope.DictionaryResources[value.name];
        });
        options = result.content.model;
    }, function() {
        console.log("Error Happened");
    });
    var linker = function(scope, element, attrs, form) {
        $timeout(function() {
            resourceService.dictionaryResource().then(function(result) {
                var template = '<select ng-init="ngModel = ngModel" name="' + scope.name + '" id="' + name + '" class="form-control"';
                template += 'ng-model="ngModel" ng-required=' + scope.required + '>';
                angular.forEach(options, function(value, key) {
                    template += '<option value="' + value.id + '" >' + value.name + '</option>'
                });
                template += '</select>';
                element.html(template).show();
                $compile(element.contents())(scope);
            }, function(error) {
                baseCommon.pushMessages(error.content.messages);
            });
        }, 100);
    };
    return {
        restrict: 'E',
        link: linker,
        scope: {
            value: '=value',
            ngModel: '=ngModel',
            name: '@name',
            required: '@required'
        }
    };
});


app.directive("selectLanguageFrom", function($timeout, $rootScope, $compile, resourceService, languageService, baseCommon) {
    var linker = function(scope, element, attrs, form) {

        var isMobile = baseCommon.isMobile();
        var limit = 2;
        var render = function($data) {
            var template = '';
            scope.languageListFrom = $rootScope.languageList;
            if (!isMobile) {
                template = '<div class="btn-group pull-left" style="z-index:9999">';
                scope.displayedLang = scope.displayedLang.length == 0 ? scope.languageListFrom : scope.displayedLang;
                angular.forEach(scope.displayedLang, function(value, key) {
                    if (key > limit) return;
                    var str = "'" + value.code + "'";
                    template += '<label id="' + attrs.id + value.code + '" ng-click="select(\'' + attrs.id + '\', \'' + value.code + '\')" class="btn btn-primary ' + attrs.id + '" ng-model="' + attrs.id + '" uib-btn-radio="' + str + '" style="margin-right:2px">' + value.name + '</label>'
                });
                template += '</div>';
                template += '<a href="" ng-click="swap()"><div class="pull-right swap-desktop"><i class="glyphicon glyphicon-retweet"></i></div></a>';
            }

            if ($rootScope.languageList.length > limit || isMobile) {
                $rootScope.dlfrom = scope.languageListFrom[0];
                if (!isMobile) {
                    template += '<ui-select ng-model="ngModel" class="col-sm-4 no-padding" on-select="selectDdl($item, $model)"> ';
                    template += '<ui-select-match placeholder="" style="width:28px"></ui-select-match>';
                } else {
                    template += '<ui-select ng-model="ngModel" class="col-xs-11 no-padding" on-select="selectDdl($item, $model)"> ';
                    template += '<ui-select-match>{{$select.selected.name}}</ui-select-match>';
                }
                template += '<ui-select-choices repeat="item.code as item in languageListFrom | filter: $select.search track by $index">';
                template += '<span ng-bind-html="item.name | highlight: $select.search"></span></ui-select-choices></ui-select>';
            }

            element.html(template).show();
            $compile(element.contents())(scope);
        }

        scope.selectDdl = function($a, $b) {
            console.log('asf');
            if($b == $rootScope.to){
                $("#to div.ui-select-container div.ui-select-match span.ui-select-match-text").text($rootScope.dlfrom.name);
                // console.log(from);
            }
            $rootScope.from = $b;
            $rootScope.dlfrom = $a;
            scope.ngModel = $b;
            if (!isMobile) {
                if($b != "id-ID" && $b != "jv-NG"){
                    scope.displayedLang[limit] = $a;
                }
                render();
            } else {
                $("#from div.ui-select-container div.ui-select-match span.ui-select-match-text").html($a.name);
            }
        }

        scope.swap = function() {
            if (!isMobile)
            {
                $rootScope.$emit('SWAP-FROM', scope.displayedLang);
            }
            var temp = $rootScope.to;
            $rootScope.to = $rootScope.from;
            $rootScope.from = temp;
            $rootScope.$broadcast('TRANSLATE');
        }

        $rootScope.$on('SWAP-TO', function(event, data) {
            scope.displayedLang = data;
            render()
        });

        if ($rootScope.languageList) {
            scope.languageListFrom = angular.copy($rootScope.languageList);
            scope.displayedLang = scope.languageListFrom;
            render();
        }

        $rootScope.$on('activeLanguageLoaded', function(event, data) {
            scope.languageListTo = angular.copy($rootScope.languageList);
            scope.displayedLang = scope.languageListTo;
            render();
        });

    };
    return {
        restrict: 'E',
        link: linker,
        scope: {
            value: '=value',
            ngModel: '=ngModel',
            name: '@name',
            required: '@required'
        },
        controller: function($scope, $element) {
            $rootScope.$watch('from', function(value, oldValue) {
                if (value == $rootScope.to && value !== undefined) {
                    $(".to").removeClass("active");
                    $("[id=to" + oldValue + "]").addClass("active");
                    $rootScope.to = oldValue;
                    $rootScope.$broadcast('TRANSLATE');
                }
                $scope.from = value;
            });
            $scope.select = function(str, id) {
                $rootScope[str] = id;
            }
        }
    };
});

app.directive("selectLanguageTo", function($timeout, $rootScope, $compile, resourceService, languageService, baseCommon) {
    var linker = function(scope, element, attrs, form) {

        var isMobile = baseCommon.isMobile();
        var limit = 2;
        var render = function() {
            var template = '';
            scope.languageListTo = $rootScope.languageList;
            if (!isMobile) {
                template = '<div class="btn-group pull-left" style="z-index:9999">';
                scope.displayedLang = scope.displayedLang.length == 0 ? scope.languageListTo : scope.displayedLang;
                angular.forEach(scope.displayedLang, function(value, key) {
                    if (key > limit) return;
                    var str = "'" + value.code + "'";
                    template += '<label id="' + attrs.id + value.code + '" ng-click="select(\'' + attrs.id + '\', \'' + value.code + '\')" class="btn btn-primary ' + attrs.id + '" ng-model="' + attrs.id + '" uib-btn-radio="' + str + '" style="margin-right:2px">' + value.name + '</label>'
                });
                template += '</div>';
            }
            if (isMobile) {
                template += '<a href="" ng-click="swap()"><div class="pull-left swap col-xs-1 no-padding" style="padding-top:7px"><i class="glyphicon glyphicon-retweet"></i></div></a>';
            }

            if ($rootScope.languageList.length > limit || isMobile) {
                $rootScope.dlto = scope.languageListTo[1];
                console.log($rootScope.dlto);
                if (!isMobile) {
                    template += '<ui-select ng-model="ngModel" class="col-sm-4 no-padding" on-select="selectDdl($item, $model)"> ';
                    template += '<ui-select-match placeholder="" style="width:28px"></ui-select-match>';
                } else {
                    template += '<ui-select ng-model="ngModel" class="col-xs-10 no-padding" on-select="selectDdl($item, $model)"> ';
                    template += '<ui-select-match>{{$select.selected.name}}</ui-select-match>';
                }
                template += '<ui-select-choices repeat="item.code as item in languageListTo | filter: $select.search track by $index">';
                template += '<span ng-bind-html="item.name | highlight: $select.search"></span></ui-select-choices></ui-select>';
            }

            element.html(template).show();
            $compile(element.contents())(scope);
        }

        scope.selectDdl = function($a, $b) {
            console.log('asf');
            if($b == $rootScope.from){
                $("#from div.ui-select-container div.ui-select-match span.ui-select-match-text").text($rootScope.dlto.name);
            }
            $rootScope.to = $b;
            $rootScope.dlto = $a;
            if (!isMobile) {
                if($b != "id-ID" && $b != "jv-NG"){
                    scope.displayedLang[limit] = $a;
                }
                render();
            } else {
                $("#to div.ui-select-container div.ui-select-match span.ui-select-match-text").html($a.name);
                $rootScope.$broadcast('TRANSLATE');
            }
        }

        scope.swap = function() {
            if (isMobile) {
                $rootScope.$emit('SWAP');
            }
            var temp = $rootScope.to;
            $rootScope.to = $rootScope.from;
            $rootScope.from = temp;
            $rootScope.$broadcast('TRANSLATE');
        }

        if ($rootScope.languageList) {
            scope.languageListFrom = angular.copy($rootScope.languageList);
            scope.displayedLang = scope.languageListFrom;
            render();
        }

        $rootScope.$on('activeLanguageLoaded', function(event, data) {
            scope.languageListTo = angular.copy($rootScope.languageList);
            scope.displayedLang = scope.languageListTo;
            render();
        });

        $rootScope.$on('SWAP-FROM', function(event, data) {
            $rootScope.$emit('SWAP-TO', scope.displayedLang);
            scope.displayedLang = data;
            render()
        });

        $rootScope.$on('SWAP', function() {
            var from = $("#from div.ui-select-container div.ui-select-match span.ui-select-match-text").text();
            var to = $("#to div.ui-select-container div.ui-select-match span.ui-select-match-text").text();
            $("#from div.ui-select-container div.ui-select-match span.ui-select-match-text").text(to);
            $("#to div.ui-select-container div.ui-select-match span.ui-select-match-text").text(from);
        });

    };
    return {
        restrict: 'E',
        link: linker,
        scope: {
            value: '=value',
            ngModel: '=ngModel',
            name: '@name',
            required: '@required'
        },
        controller: function($scope, $element) {
            $rootScope.$watch('to', function(value, oldValue) {
                // console.log('to change');
                if (value == $rootScope.from && value !== undefined) {
                    $(".from").removeClass("active");
                    $("[id=from" + oldValue + "]").addClass("active");
                    $rootScope.from = oldValue;
                    $rootScope.$broadcast('TRANSLATE');
                }
                $scope.to = value;
            });
            $scope.select = function(str, id) {
                $rootScope[str] = id;
            }
        }
    };
});

app.directive("selectLanguage", function($timeout, $rootScope, $compile, resourceService, languageService, baseCommon) {
    var linker = function(scope, element, attrs, form) {

        var isMobile = baseCommon.isMobile();
        var limit = 2;
        var render = function($isFrom) {
            console.log($isFrom);
            var template = '';
            // scope.languageList = $rootScope.languageList;
            scope.languageList = $rootScope.languageList;
            $rootScope['dl' + element[0].id] = element[0].id == 'from' ? scope.languageList[0] : scope.languageList[1];
            if (!isMobile) {
                template = '<div class="btn-group pull-left" style="z-index:9999">';
                scope.displayedLang = scope.displayedLang.length == 0 ? scope.languageListFrom : scope.displayedLang;
                scope[attrs.id] = $rootScope[attrs.id];
                angular.forEach(scope.displayedLang, function(value, key) {
                    if (key > limit) return;
                    var str = "'" + value.code + "'";
                    template += '<label id="' + attrs.id + value.code + '" ng-click="select(\'' + attrs.id + '\', \'' + value.code + '\')" class="btn btn-primary ' + attrs.id + '" ng-model="' + attrs.id + '" uib-btn-radio="' + str + '" style="margin-right:2px">' + value.name + '</label>'
                });
                template += '</div>';
            }
            if (element[0].id == 'from' && !isMobile) {
                template += '<div class="pull-right" style="padding:7px"><a href="" ng-click="swap()"><i class="glyphicon glyphicon-retweet"></i></a></div>';
            } else if (element[0].id == 'to' && isMobile) {
                template += '<div class="pull-left swap" style="padding-top:7px"><a href="" ng-click="swap()"><i class="glyphicon glyphicon-retweet"></i></a></div>';
            }

            if ($rootScope.languageList.length > limit || isMobile) {
                if (!isMobile) {
                    template += '<ui-select ng-model="ngModel" class="col-sm-4 no-padding" on-select="selectDdl($item, $model)"> ';
                    template += '<ui-select-match placeholder="" style="width:28px"></ui-select-match>';
                } else {
                    if (element[0].id == 'from') {
                        template += '<ui-select ng-model="ngModel" class="col-xs-11 no-padding" on-select="selectDdl($item, $model)"> ';
                    } else {
                        template += '<ui-select ng-model="ngModel" class="col-xs-10 no-padding" on-select="selectDdl($item, $model)"> ';
                    }

                    template += '<ui-select-match>{{$select.selected.name}}</ui-select-match>';
                }
                template += '<ui-select-choices repeat="item.code as item in languageList | filter: $select.search track by $index">';
                template += '<span ng-bind-html="item.name | highlight: $select.search"></span></ui-select-choices></ui-select>';
            }

            element.html(template).show();
            $compile(element.contents())(scope);
        }

        scope.selectDdl = function($a, $b) {
            $rootScope[element[0].id] = $b;
            scope.ngModel = $b;
            if (!isMobile) {
                if($b != "id-ID" && $b != "jv-NG"){
                    scope.displayedLang[limit] = $a;
                }
                $isFrom = element[0].id == 'from' ? true :false;
                render($isFrom);
            } else {
                $rootScope['dl' + element[0].id] = $a;
                console.log($rootScope['dl' + element[0].id]);
                if (element[0].id == 'from') {
                    $("#from div.ui-select-container div.ui-select-match span.ui-select-match-text").html($a.name);
                } else {
                    $("#to  div.ui-select-container div.ui-select-match span.ui-select-match-text").html($a.name);
                }
            }
        }

        scope.swap = function() {
            if (isMobile) {
                $rootScope.$emit('SWAP');
            }
            $isFrom = element[0].id == 'from' ? true :false;
            render($isFrom);
            var temp = $rootScope.to;
            $rootScope.to = $rootScope.from;
            $rootScope.from = temp;
            $rootScope.$broadcast('TRANSLATE');
        }

        if ($rootScope.languageList) {
            scope.languageListFrom = angular.copy($rootScope.languageList);
            scope.displayedLang = scope.languageListFrom;
            render();
        }

        $rootScope.$on('activeLanguageLoaded', function(event, data) {
            scope.languageListTo = angular.copy($rootScope.languageList);
            scope.displayedLang = scope.languageListTo;
            render();
        });

    };
    return {
        restrict: 'E',
        link: linker,
        scope: {
            value: '=value',
            ngModel: '=ngModel',
            name: '@name',
            required: '@required'
        },
        controller: function($scope, $element) {

            $rootScope.$on('SWAP', function() {
                if ($element[0].id == 'from') {
                    $rootScope.dltemp = angular.copy($rootScope.dlfrom);
                    $scope.ngModel = $rootScope.dlto.code;
                    $rootScope.dlfrom = angular.copy($rootScope.dlto);
                    $("#from div.ui-select-container div.ui-select-match span.ui-select-match-text").html($rootScope.dlto.name);
                } else {
                    $scope.ngModel = $rootScope.dltemp.code;
                    $rootScope.dlto = angular.copy($rootScope.dltemp);
                    $("#to  div.ui-select-container div.ui-select-match span.ui-select-match-text").html($rootScope.dltemp.name);
                }
            });

            $rootScope.$watch('from', function(value, oldValue) {
                if (value == $rootScope.to && value !== undefined) {
                    $(".to").removeClass("active");
                    $("[id=to" + oldValue + "]").addClass("active");
                    $rootScope.to = oldValue;
                    $rootScope.$broadcast('TRANSLATE');
                    $rootScope.$emit('SWAP');
                }
                $scope.from = value;
            });

            $rootScope.$watch('to', function(value, oldValue) {
                if (value == $rootScope.from && value !== undefined) {
                    $(".from").removeClass("active");
                    $("[id=from" + oldValue + "]").addClass("active");
                    $rootScope.from = oldValue;
                    $rootScope.$broadcast('TRANSLATE');
                }
                $scope.to = value;
            });
            $scope.select = function(str, id) {
                $rootScope[str] = id;
            }
        }
    };
});

app.filter('default', function() {
    return function(input, value) {
        return out =
            input != null && input != undefined && (input != "" || angular.isNumber(input)) ?
            input : value || '';
    }
});

app.directive('editable', function($rootScope, $compile, baseCommon, translateService) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=model',
            defaultValue: '@defaultval',
            removeFn: '=onDelete'
        },
        templateUrl: 'editable-template',
        // template: template,
        controller: function($scope) {

        },
        // The linking function will add behavior to the template
        link: function(scope, element, attrs) {
            scope.model.default = angular.copy(scope.model[0]);
            var defaultResult = scope.model.default.result;
            var match = /\r|\n/.exec(scope.model.default.result);
            if (match) {
                scope.model.default.isLineBreak = true;
            }

            scope.charCount = function($pos) {
                return new Array($pos[1] - $pos[0] - 1); //console.log($pos);
            }

            scope.starHover = function($count, $option) {
                $option.hover = [];
                for (var i = 0; i <= 5; i++) {
                    $option.hover[i] = i <= $count ? true : false;
                }
            }

            scope.selectRate = function($rates, $option) {
                var rate = {
                    //translation_id: $option.translationId,
                    first_word_id: $option.sourceId,
                    second_word_id: $option.resultId,
                    rates: $rates,
                    suggested_by: $rootScope.userId
                };
                translateService.saveRate(rate).then(function(result) {
                    baseCommon.pushToasterMessages(result.content.messages);
                }, function(error) {
                    baseCommon.pushToasterMessages(error.content.messages);
                });
            }

            scope.editorEnabled, save = false;
            scope.editableOnChange = function(keyEvent) {
                var input = element.find('input').val();
                scope.editModel = input;
                switch (keyEvent.which) {
                    case 13:
                        save = true;
                        scope.unEdit();
                        break;
                    case 27:
                        save = false;
                        scope.unEdit();
                        break;
                    default:
                        break;
                }
            }

            scope.unEdit = function($event) {
                if (save) {
                    // scope.model.default.result = angular.copy(scope.editModel);
                    // console.log(scope.model);
                    var systemSuggestions = [];
                    // var userSuggestions = [];
                    angular.forEach(scope.model, function(value, key) {
                        if (value.source != value.result) {
                            var systemSuggestion = {
                                source_id: value.sourceId,
                                source: value.source,
                                source_language_code: $rootScope.from,
                                source_prefix: value.sourcePrefix,
                                source_suffix: value.sourceSuffix,
                                source_root_id: value.sourceRootId,
                                result_id: value.sourceId,
                                result: value.result,
                                result_root_id: value.resultRootId,
                                result_language_code: $rootScope.to,
                                suggested_by: 2 // system id
                            };
                            systemSuggestions.push(systemSuggestion);
                        }
                    });
                    var userSuggestion = {
                        source_id: scope.model.default.sourceId,
                        source: scope.model.default.source,
                        source_language_code: $rootScope.from,
                        source_prefix: scope.model.default.sourcePrefix,
                        source_suffix: scope.model.default.sourceSuffix,
                        source_root_id: scope.model.default.sourceRootId,
                        result_id: scope.model.default.resultId,
                        result: scope.editModel,
                        result_root_id: scope.model.default.resultRootId,
                        result_language_code: $rootScope.to,
                        suggested_by: $rootScope.userId
                    };
                    // userSuggestions.push(userSuggestion);

                    var entity = {
                            systemSuggestions: systemSuggestions,
                            userSuggestion: userSuggestion
                        }
                        // console.log(entity);
                        // if (scope.model.default.result != scope.editModel) {
                        //
                        // var suggestedUserTranslation = {
                        //     source_id: scope.model.default.sourceId,
                        //     source: scope.model.default.source,
                        //     source_language_code: $rootScope.from,
                        //     result: scope.editModel,
                        //     result_language_code: $rootScope.to,
                        //     suggested_by: $rootScope.userId
                        // };
                        // console.log(suggestedTranslation);

                    // scope.model.unshift(userSuggestion);
                    translateService.saveSuggestion(entity).then(function(result) {
                        console.log(result.content.model);
                        scope.model = result.content.model[0];
                        scope.model.default = angular.copy(result.content.model[0][0]);
                        baseCommon.pushToasterMessages(result.content.messages);
                    }, function(error) {
                        baseCommon.pushToasterMessages(result.content.messages);
                    });
                    // }
                    save = false;
                }

                scope.inputOnBlur();
                if ($event != null) $event.preventDefault();
            };

            scope.inputOnBlur = function() {
                timer = setTimeout(function() {
                    element.find('ul.vertical-nav').hide();
                }, 0);
                scope.editorEnabled = false;
                source.highlightTextarea('destroy');
            }

            scope.enableEditor = function($event) {
                scope.model.pxwidth = $($event.currentTarget).width(); // prevent blink move left
                scope.editModel = angular.copy(scope.model.default.result);
                scope.editorEnabled = true;
                // The element enable needs focus and we wait for milliseconds before allowing focus on it.
                timer = setTimeout(function() {
                    var input = element.find('input').val(scope.editModel);
                    var pos = element.position();
                    autosizeInput(input[0]);
                    var ul = element.find('ul.vertical-nav');

                    input.focus().select();
                    ul.show();
                    ul.css({
                        top: 0,
                        left: pos.left - 16
                    });

                    scope.model.altpxwidth = ul.width();

                    source.highlightTextarea({
                        ranges: [{
                            color: '#FFFF00',
                            ranges: [scope.model[0].sourcePosition]
                        }],
                    });

                }, 10);

            };
            var source = $('#source');

            scope.hover = function($event) {
                $($event.currentTarget).addClass("result-hover");
                source.highlightTextarea({
                    ranges: [{
                        color: '#FFFF00',
                        ranges: [scope.model[0].sourcePosition]
                    }],
                });
            };

            scope.leave = function($event) {
                $($event.currentTarget).removeClass("result-hover");
                if (!$($event.toElement).is("input")) {
                    source.highlightTextarea('destroy');
                }
            };

            scope.selectWord = function(model) {
                scope.editModel = angular.copy(model.result);
                scope.model.default = angular.copy(model);
                scope.inputOnBlur();
            };

            scope.altHover = function($event, model) {
                model.hover = [];
                for (var i = 0; i <= model.rates; i++) {
                    model.hover[i] = i <= model.rates ? true : false;
                }
                scope.editModel = scope.model.default.result = angular.copy(model.result);
                var input = element.find('input').val(scope.editModel);
                autosizeInput(input[0]);
            };

            scope.altLeave = function() {
                if (scope.editorEnabled) {
                    scope.editModel = scope.model.default.result = defaultResult; //angular.copy(scope.model.default.result);
                    var input = element.find('input').val(scope.editModel);
                    autosizeInput(input[0]);
                }
            };
        }
    }
});

app.directive('booleanLabel', function($rootScope, $compile) {
    var linker = function(scope, element, attrs) {
        var yes = '<span class="label label-success">' + $rootScope.CommonResources.Yes + '</span>';
        var no = '<span class="label label-default">' + $rootScope.CommonResources.No + '</span>';
        var getTemplate = function(value) {
            var template = '';
            switch (value) {
                case "1":
                    template = yes;
                    break;
                case "0":
                    template = no;
                    break;
            }
            return template;
        };
        element.html(getTemplate(scope.value)).show();
        $compile(element.contents())(scope);
    };
    return {
        restrict: 'A',
        link: linker,
        scope: {
            value: '=value'
        }
    };
});
