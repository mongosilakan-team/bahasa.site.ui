app.constant("constants", {
    "modules": {
        resources: {
            resource: {
                api: "api/resource/"
            }
        },
        dictionary: {
            word: {
                api: "api/word/"
            },
            language: {
                api: "api/language/"
            },
            wordType: {
                api: "api/wordType/"
            },
            translation: {
                api:"api/translation/"
            }
        },
		application: {
			translate: {
				api: "api/translate/"
			}
		}
    },
    "message_type": {
        success: 'success',
        warning: 'warning',
        error: 'error',
        info: 'info'
    },
    "status": {
        active: '1',
        inactive: '2',
        pending: '3',
        review: '4'
    },
    // "language":{
    //     1 :
    // }
});
