app.factory('googleClient', googleClient);

function googleClient($q, $rootScope, $document, $window, $timeout, constants) {
    var auth2 = {};

	var googleClient = {
		startApp: _startApp,
	};
	return googleClient;

    // $rootScope.auth2 = {};
    var helper = (function() {
      return {
    	/**
    	 * Hides the sign in button and starts the post-authorization operations.
    	 *
    	 * @param {Object} authResult An Object which contains the access token and
    	 *   other authentication information.
    	 */
    	onSignInCallback: function(authResult) {
    	  $('#authResult').html('Auth Result:<br/>');
    	  for (var field in authResult) {
    		$('#authResult').append(' ' + field + ': ' +
    			authResult[field] + '<br/>');
    	  }
    	  if (authResult.isSignedIn.get()) {
    		$('#authOps').show('slow');
    		$('#gConnect').hide();
    		helper.profile();
    		helper.people();
    	  } else {
    		  if (authResult['error'] || authResult.currentUser.get().getAuthResponse() == null) {
    			// There was an error, which means the user is not signed in.
    			// As an example, you can handle by writing to the console:
    			console.log('There was an error: ' + authResult['error']);
    		  }
    		  $('#authResult').append('Logged out');
    		  $('#authOps').hide('slow');
    		  $('#gConnect').show();
    	  }

    	  console.log('authResult', authResult);
    	//   console.log(gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token);
    			// console.log();
    	},

    	/**
    	 * Calls the OAuth2 endpoint to disconnect the app for the user.
    	 */
    	disconnect: function() {
    	  // Revoke the access token.
    	  auth2.disconnect();
    	},

    	/**
    	 * Gets and renders the list of people visible to this app.
    	 */
    	people: function() {
    	  gapi.client.plus.people.list({
    		'userId': 'me',
    		'collection': 'visible'
    	  }).then(function(res) {
    		var people = res.result;
    		$('#visiblePeople').empty();
    		$('#visiblePeople').append('Number of people visible to this app: ' +
    			people.totalItems + '<br/>');
    		for (var personIndex in people.items) {
    		  person = people.items[personIndex];
    		  $('#visiblePeople').append('<img src="' + person.image.url + '">');
    		}
    	  });
    	},

    	/**
    	 * Gets and renders the currently signed in user's profile data.
    	 */
    	profile: function(){
    	  gapi.client.plus.people.get({
    		'userId': 'me'
    	  }).then(function(res) {
    		  console.log(res);
    		var profile = res.result;
    		console.log(profile);
    		$('#profile').empty();
    		$('#profile').append(
    			$('<p><img src=\"' + profile.image.url + '\"></p>'));
    		$('#profile').append(
    			$('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
    			profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
    		if (profile.emails) {
    		  $('#profile').append('<br/>Emails: ');
    		  for (var i=0; i < profile.emails.length; i++){
    			$('#profile').append(profile.emails[i].value).append(' ');
    		  }
    		  $('#profile').append('<br/>');
    		}
    		if (profile.cover && profile.coverPhoto) {
    		  $('#profile').append(
    			  $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
    		}
    	  }, function(err) {
    		var error = err.result;
    		$('#profile').empty();
    		$('#profile').append(error.message);
    	  });
    	}
      };
    })();

    /**
     * Handler for when the sign-in state changes.
     *
     * @param {boolean} isSignedIn The new signed in state.
     */
    var updateSignIn = function() {
      // console.log('update sign in state');
      if (auth2.isSignedIn.get()) {
    	//   console.log(gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token);
    	// console.log('signed in');
    	// console.log(auth2.getAuthResponse());
    	helper.onSignInCallback(gapi.auth2.getAuthInstance());
        console.log("test")

        $.post("api/translate/translate", {token: gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token}, function(result){
            // $("span").html(result
            console.log(result);
        });
        // $http({
        //     method: 'POST',
        //     url: "api/translate/",
        //     data: $.param($entity),
        //     headers: {
        //         'Content-Type': 'application/x-www-form-urlencoded'
        //     }
        // }).success(function(data) {
        //
        // }).error(function(data) {
        //
        // });

      }else{
    	// console.log('signed out');
    	helper.onSignInCallback(gapi.auth2.getAuthInstance());
      }
    }

    /**
     * This method sets up the sign-in listener after the client library loads.
     */
    function _startApp() {
      gapi.load('auth2', function() {
    	gapi.client.load('plus','v1').then(function() {
    	  gapi.signin2.render('signin-button', {
    		  scope: 'https://www.googleapis.com/auth/plus.login',
    		  fetch_basic_profile: false });
    	  gapi.auth2.init({fetch_basic_profile: false,
    		  scope:'https://www.googleapis.com/auth/plus.login'}).then(
    			function (){
    			  console.log('init');
    			  auth2 = gapi.auth2.getAuthInstance();
    			  auth2.isSignedIn.listen(updateSignIn);
    			  auth2.then(updateSignIn);
    			});
    	});
      });
    }

	// function _init() {
	// 	_populateResources();
	// 	$rootScope.$watch('resourceLoaded', function(value, oldValue) {
	// 		if ($rootScope.resourceLoaded) {
	// 			_populateLanguages();
	// 		}
	// 	});
	// }
}
