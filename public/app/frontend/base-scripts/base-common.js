app.factory('baseCommon', baseCommon);

function baseCommon($q, $rootScope, $uibModal, $document, $window, config, $timeout, constants, resourceService, languageService) {
	// $rootScope.messageIsViewed = true;
	$rootScope.messages = [];
	$rootScope.languageList = [];
	$rootScope.activeAndPendingLanguageList = [];
	$rootScope.from = 'id-ID';
	$rootScope.fromId = '1';
	$rootScope.to = 'jv-NG';
	$rootScope.toId = '2';
	// $rootScope.resourceLoaded = false;
	$rootScope.isSignedIn = false;

	$rootScope.test = function(text) {
		alert(text);
	}

	var baseCommon = {
		appInit: _appInit,
		clearErrorMessages: _clearErrorMessages,
		clearMessages: _clearMessages,
		forceCloseModal: _forceCloseModal,
		pushMessages: _pushMessages,
		pushToasterMessages: _pushToasterMessages,
		setTitle: _setTitle,
        signedIn: _signedIn,
		userSignedIn: _userSignedIn,
		isMobile: _detectMobile,
	};
	return baseCommon;

	function _init() {
		_populateResources();
		$rootScope.login = _login;
		$rootScope.$on('resourceLoaded', function(event, data) {
			_populateLanguages();
			_populateActiveAndPendingLanguages();
		});
	}

	function _detectMobile() {
		 if( navigator.userAgent.match(/Android/i)
		 || navigator.userAgent.match(/webOS/i)
		 || navigator.userAgent.match(/iPhone/i)
		 || navigator.userAgent.match(/iPad/i)
		 || navigator.userAgent.match(/iPod/i)
		 || navigator.userAgent.match(/BlackBerry/i)
		 || navigator.userAgent.match(/Windows Phone/i)
		 ){
			return true;
		  }
		 else {
			return false;
		  }
		}

	function _userSignedIn($userId, $userName, $userPicture){
		$rootScope.isSignedIn = true;
		$rootScope.userPic =  $userPicture;
		$rootScope.userName = $userName;
		$rootScope.userId = $userId;
	}


    function _signedIn($userData){
		$rootScope.$apply(function(){
			if($userData){
				$rootScope.isSignedIn = true;
				$rootScope.userPic = $userData['picture'];
				$rootScope.userName = $userData['name'];
				$rootScope.userId = $userData['id'];
			} else {
				$rootScope.isSignedIn = false;
			}
		});
		_populateLanguages();
    }

	function _login(){
		var modalInstance = $modal.open({
			animation: true,
			templateUrl: 'modal-login-template',
			controller: 'ModalLoginTemplateController',
			size: 'md',
			resolve: {
				// id: function() {
				//     return $id;
				// }
			}
		});
		modalInstance.result.then(function(result) {
			// $scope.tableParams.reload();
		}, function() {}).finally(function() {
			baseCommon.forceCloseModal();
		});
	}

	function _populateResources() {
		resourceService.getAll().then(function(result) {
			angular.forEach(result.content.model, function(resource, key) {
				$rootScope[key] = {};
				angular.forEach(resource, function(item, child_key) {
					$rootScope[key][child_key] = item = "" ? child_key : item;
				});
			});
			$rootScope.$emit('resourceLoaded', result.content.model);
		}, function(ex) {
			_debug("ERROR::When retrieving resource", 'error');
			_debug(ex, 'error');
		});
	}

	function _populateLanguages() {
		languageService.getAllUserLanguages().then(function(result) {
			angular.forEach(result.content.model, function(value, key) {
				value.name = $rootScope.DictionaryResources[value.name] === undefined ? value.name : $rootScope.DictionaryResources[value.name];
			});
			$rootScope.languageList = result.content.model;
			$rootScope.$emit('activeLanguageLoaded', result.content.model);
		}, function() {
			console.log("Error Happened");
		});
	}


	function _populateActiveAndPendingLanguages() {
		languageService.getActiveAndPending().then(function(result) {
			angular.forEach(result.content.model, function(value, key) {
				value.name = $rootScope.DictionaryResources[value.name] === undefined ? value.name : $rootScope.DictionaryResources[value.name];
			});
			$rootScope.activeAndPendingLanguageList = result.content.model;
		}, function() {
			console.log("Error Happened");
		}).finally(function() {
			$rootScope.$emit('activeAndPendingLanguageLoaded', result.content.model);
		});
	}

	function _appInit() {
		_init();
		_keyEventBinding();
		$rootScope.navigation = _getNavigation();
		// $rootScope.booleanDropdownOptions = _getBooleanDropdownOptions();
		$rootScope.sitebarAction = _sitebarAction;
		$rootScope.logout = _logout;
		// $rootScope.getStatusFilterOptions = _getStatusFilterOptions;
		// $rootScope.getBooleanFilterOptions = _getBooleanFilterOptions;
		// $rootScope.getLanguageFilterOptions = _getLanguageFilterOptions;

		$rootScope.$on('$routeChangeStart', function(event, currRoute, prevRoute) {
			$rootScope.navClass = currRoute.isHomePage ? "" : "affix";
			$rootScope.animation = currRoute.animation;
		});
	}

	function _keyEventBinding(){
        $(document).keyup(function(e) {
             if (e.keyCode == 27) { // escape key maps to keycode `27`
                _clearToaster();
            }
        });
    }

	function _clearToaster(){
		// console.log("TES");
        $rootScope.anyToaster = false;
        $("#clearToaster").hide();
        toastr.clear();
    }

	function _getBooleanFilterOptions() {
		var options = [];
		_commonResource().then(function(result) {
			options.push({
				id: "",
				title: $rootScope.CommonResources.All
			});
			options.push({
				id: "1",
				title: $rootScope.CommonResources.Yes
			});
			options.push({
				id: "0",
				title: $rootScope.CommonResources.No
			});
		}, function(error) {
			baseCommon.pushMessages(error.content.messages);
		});
		return options;
	}

	function _getBooleanDropdownOptions() {
		var options = [];
		_commonResource().then(function(result) {
			options.push({
				id: "1",
				title: $rootScope.CommonResources.Yes
			});
			options.push({
				id: "0",
				title: $rootScope.CommonResources.No
			});
		}, function(error) {
			baseCommon.pushMessages(error.content.messages);
		});
		return options;
	}

	function _commonResource() {
		var defer = $q.defer();
		$timeout(function() {
			defer.resolve($rootScope.CommonResources);
		}, 0);
		return defer.promise;
	}

	function _getStatusFilterOptions() {
		var options = [];
		_commonResource().then(function(result) {
			options.push({
				id: "",
				title: $rootScope.CommonResources.All
			});
			statusService.getAll().then(function(result) {
				angular.forEach(result.content.model, function(value, key) {
					options.push({
						id: value.id,
						title: $rootScope.ConfigurationResources[value.name]
					});
				});
			}, function(error) {
				baseCommon.pushMessages(error.content.messages);
			});
		}, function(error) {
			baseCommon.pushMessages(error.content.messages);
		});
		return options;
	}

	function _getLanguageFilterOptions() {
		var options = [];
		_commonResource().then(function(result) {
			options.push({
				id: "",
				title: $rootScope.CommonResources.All
			});
			languageService.getAll().then(function(result) {
				angular.forEach(result.content.model, function(value, key) {
					options.push({
						id: value.name,
						title: value.name
					});
				});
			}, function(error) {
				baseCommon.pushMessages(error.content.messages);
			});
		}, function(error) {
			baseCommon.pushMessages(error.content.messages);
		});
		return options;
	}

	function _logout() {
		$.get(config.base_api_url + "api/user/logout", function(data) {
			$rootScope.isSignedIn = false;
			if(gapi.auth2 === undefined){
			gapi.load('auth2', function() {
				gapi.auth2.init();
				});
			}
		$timeout(function() {
			var auth2 = gapi.auth2.getAuthInstance();
			   auth2.signOut().then(function () {
				location.reload();
			   });
			}, 500);
		});
	}

	function _setTitle(title) {
		document.title = title + " | Mongosilakan";
	}

	function _clearErrorMessages() {
		var tempMessage = angular.copy($rootScope.messages);
		$rootScope.messages.length = 0;
		angular.forEach(tempMessage, function(item, key) {
			if (item.type == constants.message_type.success) {
				$rootScope.messages.push(item);
			}
		});
	}

	function _forceCloseModal() {
		$document[0].body.classList.remove('modal-open');
		angular.element($document[0].getElementsByClassName('modal-backdrop')).remove();
		angular.element($document[0].getElementsByClassName('modal')).remove();
	}

	function _clearMessages() {
		if ($rootScope.messageIsViewed) {
			$rootScope.messages.length = 0;
		} else {
			$rootScope.messageIsViewed = true;
		}
	}

	function _pushMessages($messages) {
		_clearMessages();
		_clearMessages();
		if ($.isArray($messages)) {
			angular.forEach($messages, function(item) {
				switch (item.type) {
					case 'success':
						item.icon = 'check';
						break;
					case 'error':
						item.icon = 'ban';
						break;
				}
				$rootScope.messages.push(item);
			});
		} else {
			_debug($messages, 'error');
			// var newItem = {
			//     type: 'error',
			//     message: $messages
			// };
			// $rootScope.messages.push(newItem);
		}
		$('html, body').animate({
			scrollTop: 0
		}, 'fast');
		$rootScope.messageIsViewed = false;
	}

	function _pushToasterMessages($messages) {
		_clearMessages();
		if ($.isArray($messages)) {
			// angular.forEach($messages, function(item) {
			// 	$timeout(function() {
			// 		_toastr(item.message, item.type);
			// 	}, 1000);
			// });
			var left = $messages.length;
			var start = 0;
			var toast = function (){
				// left--;
				_toastr($messages[start].message, $messages[start].type);
				start++;
				if(start != left) $timeout(toast, 300);
			}
			$timeout(toast, 300)
		}
	}

	function _getNavigation() {
		var navigation = {
			// dashboard: {
			//     label: "Dashboard",
			//     status: "active",
			//     url: "dashboard",
			//     icon: "fa-dashboard"
			// },
			dictionary: {
				label: "Dictionary",
				url: "dictionary",
				hasSubs: true,
				icon: "fa-book",
				subs: {
					// words: {
					//     label: "WordManagement",
					//     url: "dictionary/words",
					//     icon: "fa-sort-alpha-asc",
					// },
					translation: {
						label: "Translations",
						url: "dictionary/translations",
						icon: "fa-language",
					},
					// languages: {
					//     label: "Languages",
					//     url: "dictionary/languages",
					//     icon: "fa-language",
					// },
					// wordTypes: {
					//     label: "WordTypes",
					//     url: "dictionary/wordTypes",
					//     icon: "fa-language",
					// }
				}
			},
			// donation: {
			//     label: "Donation",
			//     url: "donation",
			//     hasSubs: true,
			//     icon: "fa-money",
			//     subs: {
			//         donors: {
			//             label: "Donors",
			//             url: "donation/donors",
			//             icon: "fa-group",
			//         },
			//         donations: {
			//             label: "Donations",
			//             url: "donation/donations",
			//             icon: "fa-credit-card",
			//         }
			//     }
			// },
			// configurations: {
			//     label: "Configurations",
			//     hasSubs: true,
			//     icon: "fa-cogs",
			//     subs: {
			//         users: {
			//             label: "Users",
			//             url: "configurations/users",
			//             icon: "fa-users",
			//         },
			//         statuses: {
			//             label: "Statuses",
			//             url: "configurations/statuses",
			//             icon: "fa-flag",
			//         },
			//         system: {
			//             label: "System",
			//             url: "configurations/settings",
			//             icon: "fa-wrench",
			//         }
			//     }
			// }
		};
		return navigation;
	}

	function _shortcut() {
		_debug($rootScope.shortcutCommand);
	}

	function _sitebarAction($event, $isChild) {
		if (!angular.element($event.currentTarget).find('ul').length) {
			angular.element($event.currentTarget).parent().find('li.active').removeClass('active');
			angular.element($event.currentTarget).addClass('active');
		}
		if ($isChild) {
			angular.element($event.currentTarget).parent().parent().parent().find('li.active').removeClass('active');
			angular.element($event.currentTarget).parent().find('li.active').removeClass('active');
			angular.element($event.currentTarget).parent().parent().addClass('active');
			angular.element($event.currentTarget).parent().addClass('active');
			angular.element($event.currentTarget).addClass('active');
		}
	}
}
// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	};
}

function _toastr($message, $type) {

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "3000",
		"timeOut": 10000,
		"extendedTimeOut": 1000,
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};
	if($message.charAt(0) == "\"") $message = $message.slice(1, -1);
	switch ($type) {
		case 'error':
			toastr.error($message);
			break;
		case 'info':
			toastr.info($message);
			break;
		case 'warning':
			toastr.warning($message);
			break;
		case 'success':
			toastr.success($message);
			break;
		default:
			toastr.info($message);
			break;
	}
}

function _debug($message, $type) {
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-bottom-full-width",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": 0,
		"extendedTimeOut": 0,
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut",
		"tapToDismiss": false
	};
	console.log($message.charAt(0));
	if($message.charAt(0) == "\"") $message = $message.slice(1, -1);
	switch ($type) {
		case 'error':
			toastr.error(JSON.stringify($message));
			break;
		case 'info':
			toastr.info(JSON.stringify($message));
			break;
		case 'warning':
			toastr.warning(JSON.stringify($message));
			break;
		case 'success':
			toastr.success(JSON.stringify($message));
			break;
		default:
			toastr.info(JSON.stringify($message));
			break;
	}

    $("#toast-container").css("max-height","100%");
	$("#toast-container").css("overflow-y","scroll");
	$("#toast-container").css("pointer-events","auto");
    $("#toast-container>div, a").css("color","black");
}
