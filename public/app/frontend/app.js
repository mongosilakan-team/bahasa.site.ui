var app = angular.module("app", ["angular-loading-bar", 'ngSanitize', 'ui.select', "ngRoute", "ui.bootstrap", "ngAnimate", "ngTable", "applicationModule", 'angulartics', 'angulartics.google.analytics']);
app.config(['$routeProvider', 'cfpLoadingBarProvider',
    function($routeProvider, cfpLoadingBarProvider) {
        $routeProvider.otherwise({
            redirectTo: "/translate",
        });
        // $modalProvider.options.animation = false;
        cfpLoadingBarProvider.includeSpinner = false;
    }
]);
app.run(function(baseCommon) {
    baseCommon.appInit();
});

app.filter('getById', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return input[i];
      }
    }
    return null;
  }
});

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
