applicationModule.service('translateService', translateService);

function translateService($http, $q, config, constants, baseCommon) {
    var translateService = {
        cancel: cancel,
        getApi: _getApi,
        translate: _translate,
        saveSuggestion: _saveSuggestion,
        saveRate: _saveRate,
        selectAlt: _selectAlt,
        getWordDictionaryDetails: _getWordDictionaryDetails,
        getSuggestionWordDictionary: _getSuggestionWordDictionary,
    };
    // runServices = angular.extend(angular.copy(baseService), runServices);
    return translateService;

    function _getApi() {
        return constants.modules.application.translate.api;
    }

    function cancel(promise) {
        // If the promise does not contain a hook into the deferred timeout,
        // the simply ignore the cancel request.
        if (
            promise &&
            promise._httpTimeout &&
            promise._httpTimeout.resolve
        ) {
            promise._httpTimeout.resolve();
        }
    }

    function _translate($entity) {
        var defer = $q.defer();

        var request = $http({
            method: "POST",
            url: config.base_api_url + _getApi() + "translate",
            data: $.param($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            timeout: defer.promise
        });

        var promise = request.then(unwrapResolve);

        promise._httpTimeout = defer;

        return (promise);
    }

    function _translateGet($from, $to, $source) {
        var defer = $q.defer();

        var request = $http({
            method: "GET",
            url: config.base_api_url + _getApi() + "translate" + "?from=" + $from + " &to=" + $to + " &source=" + escape($source),
            timeout: defer.promise
        });

        var promise = request.then(unwrapResolve);

        promise._httpTimeout = defer;

        return (promise);
    }

    function unwrapResolve(response) {
        return (response.data);
    }

    function _saveSuggestion($entity) {
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: config.base_api_url + this.getApi() + "saveSuggestion",
            data: $.param($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if (data.content === undefined) {
                _debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                baseCommon.pushToasterMessages([{
                    message: data.content.messages,
                    type: 'warning'
                }]);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }

    function _saveRate($entity) {
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: config.base_api_url + this.getApi() + "saveRate",
            data: $.param($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if (data.content === undefined) {
                _debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                baseCommon.pushToasterMessages([{
                    message: data.content.messages,
                    type: 'warning'
                }]);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }

    function _selectAlt() {

    }

    function _getSuggestionWordDictionary($val, $lang) {
        $entity = {
            value: $val,
            lang: $lang
        };
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: config.base_api_url + constants.modules.dictionary.word.api + "getSuggestionWordDictionary",
            data: $.param($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if (data.content === undefined) {
                _debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                baseCommon.pushToasterMessages([{
                    message: data.content.messages,
                    type: 'warning'
                }]);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }

    function _getWordDictionaryDetails($id) {
        $entity = {
            id: $id
        };
        var defer = $q.defer();
        $http({
            method: 'POST',
            url: config.base_api_url + this.getApi() + "getWordDictionaryDetails",
            data: $.param($entity),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            if (data.content === undefined) {
                _debug(data);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).error(function(data) {
            if (data.content.errorCode == 401) {
                baseCommon.pushToasterMessages([{
                    message: data.content.messages,
                    type: 'warning'
                }]);
            } else {
                defer.reject(data);
            }
        });
        return defer.promise;
    }

    // function _translate($from, $to, $source) {
    //     var defer = $q.defer();
    //     $http({
    //         method: 'GET',
    //         url: config.base_api_url + _getApi() + "translate" + "?from="+ $from +" &to="+ $to +" &source=" + escape($source)
    //     }).success(function(data) {
    //     	if (data.content === undefined) {
    //             defer.reject(data);
    //         } else {
    //             defer.resolve(data);
    //         }
    //     }).error(function(data) {
    //         defer.reject(data);
    //     });
    //     return defer.promise;
    // }
}
